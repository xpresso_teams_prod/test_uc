import os
import sys
import json
import types
import pickle
import marshal
import datetime
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow_hub as hub
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType
## $xpr_param_component_name = my_new_comp
## $xpr_param_component_type = pipeline_job 
## $xpr_param_global_variables = []
## $xpr_param_global_methods = []
hub_url = "https://tfhub.dev/google/universal-sentence-encoder/4"
embed = hub.KerasLayer(hub_url)
path = '/opt/jupyter/data/processed_data'
files = os.listdir(path)
# sgsg
#s srse
plans = {}
columns = ['Plan_Name', 'Plan_Type', 'Important Questions', 'Answers', 'Why this Matters:', 'Url']
df = pd.DataFrame(columns=columns)
counter = 0
for file in files:
    with open(path + '/' +file,'r') as filen:
        f = json.load(filen)
        plan_name = f['Plan_Name'].strip()
        plan_type = f['Plan_Type'].strip()
        for rows in f['Page_1']:
            for each_row in rows:
                ques = f['Page_1'][each_row]['Important Questions']
                ques = ques.replace("\n" ," ")
                ques = ques.replace("–"," ")
                ques = ques.replace("-"," ")
                ques = ques.replace("?", "")
                ques = ques.strip()
                ans = f['Page_1'][each_row]['Answers'].replace("\n"," ")
                ans = ans.strip()
                why = f['Page_1'][each_row]['Why this Matters:'].replace("\n"," ")
                why = why.strip()
                file_url = file.replace("%","%25")
                url = 'http://172.16.6.95:6080/files/data/Ambetter_pdf/'+file_url.split("json")[0] + 'pdf'
                df.loc[counter] = [plan_name] + [plan_type] + [ques] +[ans] + [why] + [url]
                counter = counter + 1
df.head()
questions = df['Important Questions'].tolist()
embeddings = embed(questions)
type(embeddings)
embeddings
#Request
Plan_Name = "Ambetter Platinum 1"
Plan_Type = "HMO"
query = "out of pcoket"
start = datetime.datetime.now()
index_list = df.index[(df['Plan_Name'] == Plan_Name) & (df['Plan_Type'] == Plan_Type)].tolist()
query_embed = embed([query])
similarity = np.inner(query_embed,embeddings)
max_index = np.argmax(similarity[:,index_list], axis = 1)
df_index = index_list[max_index[0]]
end = datetime.datetime.now()
diff = end - start
elapsed_ms = (diff.days * 86400000) + (diff.seconds * 1000) + (diff.microseconds / 1000)
print("Query processed in time in milliseconds :", elapsed_ms)
print("Plan_Name : ",df.loc[df_index]['Plan_Name'])
print("Plan_Type : ",df.loc[df_index]['Plan_Type'])
#print("Question : ",df.loc[df_index]['Important Questions'])
print("Answers : ",df.loc[df_index]['Answers'])
#print("Why this Matters: : ",df.loc[df_index]['Why this Matters:'])
print("Url: ",df.loc[df_index]['Url'])
